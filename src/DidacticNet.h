/**************************************************************************
	@file     DidacticNet.h
	@author   anian buehler @ letsgoING
**************************************************************************/

#ifndef _DIDACTICNET_
#define _DIDACTICNET_

#include "Arduino.h"

#ifdef CALLBACK_W_LENGTH
// callback(topic, topicLength, payload, payloadLength)
#define PSNET_CALLBACK_SIGNATURE void (*callback)(char *, int, char *, int)
#else
// callback(topic, payload)
#define PSNET_CALLBACK_SIGNATURE void (*callback)(char *, char *)
#endif

#define CLIENT_MODE_BASIC false
#define CLIENT_MODE_ADVANCED true

#define MSG_PRELIMITER '<'
#define MSG_DELIMITER '>'
#define MSG_SEPARATOR '|'

//@ publish   → on publish check topic, then send topic-update
//? subscribe → subscribe starts update, topic filter @client
// # update    → update to specific topic Broker to client
#define MSG_PUBLISH '@'
#define MSG_SUBSCRIBE '?'
#define MSG_UPDATE '#'
#define MSG_TOPIC_MULTI '*'

//<@topic|payload>
#define LEN_OVERHEAD 4

#define CSMA_CHECK_DELAY_US 400
#define CSMA_MIN_DELAY_MS 10
#define CSMA_MID_DELAY_MS 20
#define CSMA_MAX_DELAY_MS 30

#define INTERVAL_CLIENT 500L
#define INTERVAL_BROKER 0L

#define MAX_NR_TOPICS_CLIENT 10
#define MAX_NR_TOPICS_BROKER 20
#define MAX_LEN_TOPICS 10
#define MAX_LEN_PAYLOAD 20
#define MAX_LEN_USERINPUT 41

#define DN_PUBLISH_SUCCESSULL 1
#define DN_ERROR_NO_ERROR 0
#define DN_ERROR_TOPIC_LEN -1
#define DN_ERROR_PAYLOAD_LEN -2
#define DN_ERROR_NO_TOPIC -3

// little helpers
#define DN_ASCII_EOS 0
#define DN_ASCII_CR 13
#define DN_ASCII_NL 10
#define DN_ASCII_DEL 127

class EdgeDetector
{
private:
public:
	EdgeDetector();
	~EdgeDetector();
	int edgeDetected(bool);
};

class ChangeDetector
{
private:
public:
	ChangeDetector();
	~ChangeDetector();
	bool valueChanged(int, int);
};

class UnblockingTimer
{
private:
	long lastTime = 0L;

public:
	UnblockingTimer();
	~UnblockingTimer();
	bool timeElapsed(long);
};
class SerialReader
{
protected:
	Stream *_port;
	int charCounter = 0;

public:
	SerialReader();
	~SerialReader();
	void begin(Stream &);
	int readSerialData(char *, char);
};

class serialMonitorUI : public SerialReader
{
protected:
	char _currentCommand = '\0';
	char _currentInput[MAX_LEN_USERINPUT + 1] = {'\0'};

	char extractCommand(char *);

public:
	serialMonitorUI();
	~serialMonitorUI();
	bool available();
	char readCommand();
	void readUserInput(char *);
};

class DidacticPSNet
{
protected:
	Stream *_port;

	PSNET_CALLBACK_SIGNATURE;

	char _bufferTopic[MAX_LEN_TOPICS + 1] = {0};
	char _bufferPayload[MAX_LEN_PAYLOAD + 1] = {0};
	char _readBufferMessage[MAX_LEN_TOPICS + MAX_LEN_PAYLOAD + LEN_OVERHEAD + 1];
	char _sendBufferMessage[MAX_LEN_TOPICS + MAX_LEN_PAYLOAD + LEN_OVERHEAD + 1];

	bool _dataToSend = false; // int Data to send for queue?
	unsigned long _waitingTimeSend = 0L;
	unsigned long _waitingTimeCSMA = 0L;
	unsigned long _intervalTime = 0L;
	int _currentTopicLength = 0;
	int _currentPayloadLength = 0;

	DidacticPSNet &setCallback(PSNET_CALLBACK_SIGNATURE);

	void setStream(Stream &_port);

	int checkData();
	bool recieveData();
	bool sendData();
	int extractData(int, int, char *, char);
	void writeDataToTopic(char *, char *);
	virtual int getTopicNr(char *) = 0;
	virtual int getFreeTopicNr() = 0;
	virtual bool getMessageFilter(char) = 0;
	virtual bool savePayload(char *, int) = 0;
	virtual bool handleData() = 0;

public:
	DidacticPSNet();
	~DidacticPSNet();

	void begin(Stream &_port);
	void begin(Stream &_port, PSNET_CALLBACK_SIGNATURE);

	bool handleNetwork();
	bool isDataToSend();

	void setInterval(long);
};

class DidacticPSNetClient : public DidacticPSNet
{
private:
	EdgeDetector eDetector;
	ChangeDetector cDetector;

	char _topic[MAX_NR_TOPICS_CLIENT][MAX_LEN_TOPICS + 1] = {{0}};
	char _payload[MAX_NR_TOPICS_CLIENT][MAX_LEN_PAYLOAD + 1] = {{0}};

	bool _clientMode = CLIENT_MODE_ADVANCED;
	bool _newMessageAvailable = false;
	int _newMessageTopicNr = 0;
	char _currentTopic[MAX_LEN_TOPICS + 1] = {0};
	char _currentPayload[MAX_LEN_PAYLOAD + 1] = {0};

	bool savePayload(char *, int);
	bool getMessageFilter(char);
	bool handleData();
	int getTopicNr(char *);
	int getFreeTopicNr();

	int edgeDetected(bool);
	bool valueChanged(int, int);

public:
	DidacticPSNetClient();
	~DidacticPSNetClient();

	void begin(Stream &_port);
	void begin(Stream &_port, PSNET_CALLBACK_SIGNATURE);

	bool available();
	int readLatestTopicNr();
	void readTopic(char *);
	void readTopic(int, char *);
	void readPayload(char *);
	void readPayload(int, char *);
	void readPayload(char *, char *);
	bool readBooleanPayload();
	int readIntegerPayload();

	int getMaxNrTopics();
	int getSubscribedTopic(char *, int);

	int publish(char *, char *);
	int publish(char *, int, char *, int);
	int publish(char *, int);
	int publish(char *, bool);
	int publishOnChange(char *, bool);
	int publishOnChange(char *, int, int);
	int subscribe(char *);
	int subscribe(char *, int);
	bool unsubscribe(char *);
	bool unsubscribe(char *, int);
};

class DidacticPSNetBroker : public DidacticPSNet
{
private:
	Stream *_verbosePort;
	char _topic[MAX_NR_TOPICS_BROKER][MAX_LEN_TOPICS + 1] = {{0}};
	char _data[MAX_NR_TOPICS_BROKER][MAX_LEN_PAYLOAD + 1] = {{0}};

	bool _isVerbose = false;

	bool savePayload(char *, int);
	bool getMessageFilter(char);
	void writeDataToTopic(int, char *, char *);
	bool handleData();
	int getTopicNr(char *);
	int getFreeTopicNr();
	bool update(char *, int, char *, int);
	bool printVerbose(char *);

public:
	DidacticPSNetBroker();
	~DidacticPSNetBroker();

	void setVerbose(Stream &_port);
	void setNoneVerbose();
};

#endif
