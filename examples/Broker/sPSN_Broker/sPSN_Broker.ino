/**
 *file:  sPSN_Broker.ino
 *author:  letsgoING -> info@letsgoing.de
 *
 *description:
 * Dieses Programm ist ein Teil eines Beispiels für ein einfaches Pub-Sub-Netzwerk.
 *
 * Für ein Sensor-Netwerk werden 3 Arduinos mit IR-Link-Modulen benötigt:
 * Arduino1: sPSN_Broker.ino (dieses Programm)
 *           - IR-Sender an Pin 11 IR-Empfänger an Pin10
 * Arduino2: sPSN_Client1.ino
 *           - IR-Sender an Pin 11 IR-Empfänger an Pin10
 *           - Taster an Pin2
 *           - LED an Pin9
 * Arduino3: sPSN_Client2.ino
 *           - IR-Sender an Pin 11 IR-Empfänger an Pin10
 *           - Poti an PinA0
 *           - LED an Pin9
 *
 * Für ein Chat-Netzwerk werden mindestens 3 Arduinos benötigt:
 * Arduino1:   sPSN_Broker.ino (dieses Programm)
 *            - IR-Sender an Pin 11 IR-Empfänger an Pin10
 * Arduino2-n: sPSN_Chat.ino
 *            - IR-Sender an Pin 11 IR-Empfänger an Pin10
 *
 *date:  14.12.2020
 *version: 1.0
 */
#include <Arduino.h>
#include "SoftwareSerial.h"
#include "DidacticNet.h"

// lege Geschwindigkeit für serielle Schnittstellen fest
#define SERIAL_BAUD 2400

// lege Pins für SoftwareSerielle Schnittstelle fest
//  Rx = 10 -> Empfänger | Tx = 11 -> Sender
SoftwareSerial sSerial(10, 11);

// Erzeuge Broker-Instanz
DidacticPSNetBroker psnBroker;

void setup()
{
  // Starte SoftwareSerielle Schnittstelle (zu IR-Link-Modulen)
  sSerial.begin(SERIAL_BAUD);

  // Lege fest welche Serielle Schnittstelle für sPSN verwendet werden soll
  // psnBroker.setStream(sSerial);
  psnBroker.begin(sSerial);
}

void loop()
{
  // hier verarbeitet der Broker alle Daten
  psnBroker.handleNetwork();
}
