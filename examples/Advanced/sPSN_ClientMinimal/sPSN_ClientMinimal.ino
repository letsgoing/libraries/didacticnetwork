/**
 *file:  sPSN_Client1
 *author:  letsgoING -> info@letsgoing.de
 *
 *description:
 * Dieses Programm ist das Minimal-Beispiel für ein einfaches Pub-Sub-Netzwerk.
 * Es wird jede Sekunde ein analoger Messwert (AO) übertragen 
 * und die empfangenen Daten auf dem SerialMonitor ausgegeben
 * 
 * Für das Netwerk werden 3 oder mehr Arduinos mit IR-Link-Modulen benötigt:
 *
 * Arduino1: sPSN_Broker.ino
 * 
 * Arduino2-n: sPSN_ClientMinimal.ino (dieses Programm)  
 * 
 * parameter:
 *  MAX_LEN_PAYLOAD = 20 Zeichen (didacticNet.h)
 *  MAX_LEN_TOPICS = 10 Zeichen (didacticNet.h)
 * 
 *date:  06.07.2021
 */
#include <Arduino.h>

#include "SoftwareSerial.h"
#include "DidacticNet.h"

SoftwareSerial sSerial(10, 11); //Erzeuge SoftwareSerial-Instanz mit Rx = Pin10 -> Empfänger | Tx = Pin11 -> Sender

DidacticPSNetClient psnClient;  //Erzeuge PubSub-Client-Instanz

//Callback-Funktion - wird beim Empfang neuer Daten aufgerufen
void newData(char* topic, char* payload) {
  Serial.print("Topic: ");
  Serial.print(topic);
  Serial.print("   Nutzdaten: ");
  Serial.println(payload);
}


void setup() {
  
  Serial.begin(2400);    //Starte Serielle Schnittstelle (zum PC)
  sSerial.begin(2400);   //Starte SoftwareSerielle Schnittstelle (zu IR-Link-Modulen)

  psnClient.begin(sSerial, newData); //Starte PubSub Client an SoftwareSerial Schnittstelle
  
  //Hier EMPFANGS-TOPIC ANPASSEN -> default "client2"
  psnClient.subscribe("client2");           //Lege fest zu welchem Topic Daten empfangen werden sollen
}

void loop() {

  psnClient.handleNetwork();         //Verarbeiten der Daten, prüfen ob Netzwerk frei und versenden der Daten

  int currentValue = analogRead(A0); //lese Poti ein und speichere Wert
  
  //Hier SENDE-TOPIC ANPASSEN -> default "client1"
  psnClient.publish("client1", currentValue);
}

