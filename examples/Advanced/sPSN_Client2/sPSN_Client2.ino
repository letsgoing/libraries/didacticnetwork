/**
 *file:  sPSN_Client1
 *author:  letsgoING -> info@letsgoing.de
 *
 *description:
 * Dieses Programm ist ein einfaches Beispiel für ein einfaches Pub-Sub-Netzwerk.
 * Für das Netwerk werden 3 Arduinos mit IR-Link-Modulen benötigt:
 *
 * Arduino1: sPSN_Broker.ino
 *
 * Arduino2: sPSN_Client1.ino -> Sendet Wert des Potis | Empfängt Zustand des Tasters
 * (Poti an PinA0 | LED an Pin5)
 *
 * Arduino3: sPSN_Client2.ino (dieses Programm) -> Sendet Zustand des Tasters | Empfängt Wert des Potis
 * (Taster an Pin2 | LED an Pin5)
 *
 *date:  06.07.2021
 */

#include <Arduino.h>
#include "SoftwareSerial.h"
#include "DidacticNet.h"

#define SERIAL_BAUD 2400 // lege Geschwindigkeit für serielle Schnittstellen fest

#define LED_PIN 5
#define LED2_PIN 6
#define BUTTON_PIN 2

char topicPublish[MAX_LEN_TOPICS] = "btnNr";    // Topic unter dem (eigene) Daten veröffentlicht werden
char topicSubscribe[MAX_LEN_TOPICS] = "potiNr"; // Topic (von anderem TN) das abboniert werden soll

SoftwareSerial sSerial(10, 11); // Erzeuge SoftwareSerial-Instanz mit Rx = Pin10 -> Empfänger | Tx = Pin11 -> Sender

DidacticPSNetClient psnClient; // Erzeuge PubSub-Client-Instanz

// Callback-Funktion - wird beim Empfang neuer Daten aufgerufen
void newData(char *topic, char *payload)
{
  Serial.print("New data from topic ");
  Serial.print(topic);
  Serial.print(": ");
  Serial.println(payload);

  int valuePoti = atoi(payload); // wandle ASCII-Zeichen in Wert
  // Alternativ: sscanf(mData, "%d", &stateLED); //wandle ASCII-Zeichen in Wert

  int mappedValue = map(valuePoti, 0, 1023, 0, 255);
  int valueLED = constrain(mappedValue, 0, 255); // passe analogRead-Wert (0-1023) für analogWrite (0-255) an und begrenze

  analogWrite(LED_PIN, valueLED);        // Setze Ausgang entsprechend dem empfangenen Wert
  analogWrite(LED2_PIN, 255 - valueLED); // Setze Ausgang invertiert zum empfangenen Wert
}

void setup()
{

  Serial.begin(SERIAL_BAUD);  // Starte Serielle Schnittstelle (zum PC)
  sSerial.begin(SERIAL_BAUD); // Starte SoftwareSerielle Schnittstelle (zu IR-Link-Modulen)

  psnClient.begin(sSerial, newData); // Starte PubSub Client an SoftwareSerial Schnittstelle

  psnClient.subscribe(topicSubscribe); // Lege fest zu welchem Topic Daten empfangen werden sollen
}

void loop()
{

  psnClient.handleNetwork(); // Verarbeiten der Daten, prüfen ob Netzwerk frei und versenden der Daten

  boolean buttonState = digitalRead(BUTTON_PIN);        // lese Taster ein und speichere Wert
  psnClient.publishOnChange(topicPublish, buttonState); // bereite Topic und Nutzdaten zum senden vor, wenn sich buttonState ändert
}
