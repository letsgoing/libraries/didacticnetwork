/**
 *file:  sPSN_PingTest
 *author:  letsgoING -> info@letsgoing.de
 *
 *description:
 * Test-Software um Clients im Netzwerk "anzumelden"
 * Für das Netwerk werden min. 3 Arduinos mit IR-Link-Modulen benötigt:
 *
 * Arduino1: sPSN_Broker.ino
 * 
 * Arduino2: sPSN_PongTester.ino -> fragt Clients ab
 * 
 * Arduino3: sPSN_PingTest.ino -> reagiert auf PongTester
 * 
 *date:  04.07.2022
 */

#include "Arduino.h"

#include "SoftwareSerial.h"
#include "DidacticNet.h"

//lege Geschwindigkeit für serielle Schnittstellen fest
#define SERIAL_BAUD 2400

//HIER CLIENT-NUMMER ANPASSEN
//*******************************
#define MY_NUMBER  1
//*******************************

#define LED_PIN    5
#define LED2_PIN   6
#define BUTTON_PIN 2


//lege Pins für SoftwareSerielle Schnittstelle fest
// Rx = 10 -> Empfänger | Tx = 11 -> Sender
SoftwareSerial sSerial(10, 11);

//Erzeuge Client-Instanz
DidacticPSNetClient psnClient;

EdgeDetector eDetector;

//Arrays für Empfangs- und Sende Topic
char topicSub[MAX_LEN_TOPICS] = "";
char topicPub[MAX_LEN_TOPICS] = "";

char payloadSend[] = "Ping";

bool ledState = true;

void newData(char* topic, char* payload) {
  if (!strcmp(payload, "Ping")){
    Serial.print(payload);
    Serial.print(" -> ");
  } else{
    Serial.print(payload);
    Serial.println(" :-D");
  } 
  digitalWrite(LED_BUILTIN, HIGH);
}


void setup() {
  //Starte Serielle Schnittstelle (zum PC)
  Serial.begin(SERIAL_BAUD);
  delay(2000);

  //Verwende eigene Nummer als Sende- und Empfangstopic 
  //(2-stellig mit führender 0)
  sprintf(topicPub, "%02d", MY_NUMBER);
  sprintf(topicSub, "%02d", MY_NUMBER);

  Serial.print("\nStarting sPSNet as Client ");
  Serial.println(topicPub);


  //Starte SoftwareSerielle Schnittstelle (zu IR-Link-Modulen)
  sSerial.begin(SERIAL_BAUD);

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(LED2_PIN, OUTPUT);

  digitalWrite(LED_BUILTIN, LOW);
  digitalWrite(LED_PIN, ledState);
  digitalWrite(LED2_PIN, ledState);

  psnClient.begin(sSerial, newData);

  psnClient.publish(topicPub, payloadSend);
  while (!psnClient.handleNetwork());

  psnClient.subscribe(topicSub);
  while (!psnClient.handleNetwork());

  psnClient.setInterval(5000L);
}

void loop() {
  //Verarbeiten der Daten, prüfen ob Netzwerk frei und versenden der Daten
  psnClient.handleNetwork();

  psnClient.publish(topicPub, payloadSend);

  if (eDetector.edgeDetected(digitalRead(BUTTON_PIN)) == RISING) {
    ledState = !ledState;
    digitalWrite(LED_PIN, !ledState);
    digitalWrite(LED2_PIN, ledState);
  }
}