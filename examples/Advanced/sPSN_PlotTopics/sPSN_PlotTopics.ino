/**
 *file:  sPSN_PlotTopics
 *author:  letsgoING -> info@letsgoing.de
 *
 *description:
 * Dieses Programm abboniert Beispiel-Topics und gibt diese auf dem SerialMonitor aus.
 *
 * parameter:
 *  MAX_LEN_PAYLOAD = 20 Zeichen (didacticNet.h)
 *  MAX_LEN_TOPICS = 10 Zeichen (didacticNet.h)
 * 
 *date:  15.07.2022
 */
#include <Arduino.h>
#include "SoftwareSerial.h"
#include "DidacticNet.h"

#define SERIAL_BAUD 2400 //lege Geschwindigkeit für serielle Schnittstellen fest

SoftwareSerial sSerial(10, 11); //Erzeuge SoftwareSerial-Instanz mit Rx = Pin10 -> Empfänger | Tx = Pin11 -> Sender

DidacticPSNetClient psnClient;  //Erzeuge PubSub-Client-Instanz


//Callback-Funktion - in diesem Beispiel nicht verwendet
void newData(char* topic, char* payload) {}


void setup() {
  
  Serial.begin(SERIAL_BAUD);    //Starte Serielle Schnittstelle (zum PC)
  sSerial.begin(SERIAL_BAUD);   //Starte SoftwareSerielle Schnittstelle (zu IR-Link-Modulen)

  psnClient.begin(sSerial, newData); //Starte PubSub Client an SoftwareSerial Schnittstelle

  //Beliebige Beispiel-Topics abbonieren -> zusätzliche Topics hinzufügen
  psnClient.subscribe("temp1");
  psnClient.subscribe("temp2");
  psnClient.subscribe("light1");
  psnClient.subscribe("poti");

  //lese Anzahl der abbonierten Topics aus
  int countTopics =  psnClient.getMaxNrTopics();
  Serial.print("Anzahl Topics: ");
  Serial.println(countTopics);

  //Gebe alle Topics nacheinander aus
  for(int i = 0; i < countTopics; i++){
    char topic[MAX_LEN_TOPICS];
    psnClient.getSubscribedTopic(topic, i);
    
    Serial.print("Topic ");
    Serial.print(i);
    Serial.print(": ");
    Serial.println(topic);
  }
}

void loop() {
}
