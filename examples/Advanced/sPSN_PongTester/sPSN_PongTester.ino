
/**
 *file:  sPSN_PongTester
 *author:  letsgoING -> info@letsgoing.de
 *
 *description:
 * Test-Software um Clients im Netzwerk "anzumelden"
 * Für das Netwerk werden min. 3 Arduinos mit IR-Link-Modulen benötigt:
 *
 * Arduino1: sPSN_Broker.ino
 * 
 * Arduino2: sPSN_PongTester.ino -> fragt Clients ab
 * 
 * Arduino3: sPSN_PingTest.ino -> reagiert auf PongTester
 * 
 *date:  04.07.2022
 */

#include "Arduino.h"

#include "SoftwareSerial.h"
#include "DidacticNet.h"

//lege Geschwindigkeit für serielle Schnittstellen fest
#define SERIAL_BAUD 2400

//HIER CLIENT_ZAHL ANPASSEN
//*******************************
#define MAX_NR_CLIENTS 15
//*******************************

//lege Pins für SoftwareSerielle Schnittstelle fest
// Rx = 10 -> Empfänger | Tx = 11 -> Sender
SoftwareSerial sSerial(10, 11);

//Erzeuge Client-Instanz
DidacticPSNetClient psnClient;

bool recievedPings[MAX_NR_CLIENTS+1] = {false}; //+1 ermöglicht Start bei Client0 oder Client1 
bool newPing = false;

void newData(char* topic, char* payload) {
  if (!strcmp(payload, "Ping") && (atoi(topic) < MAX_NR_CLIENTS+1)) { //Empfangene Nachricht == Ping
    recievedPings[atoi(topic)] = true;
    newPing = true;
    psnClient.publish(topic, "Pong");
  }
}

void setup() {
  //Starte Serielle Schnittstelle (zum PC)
  Serial.begin(SERIAL_BAUD);
  delay(2000);

  Serial.print("\nSearching for ");
  Serial.print(MAX_NR_CLIENTS);
  Serial.println(" sPSNet-Clients");

  //Starte SoftwareSerielle Schnittstelle (zu IR-Link-Modulen)
  sSerial.begin(SERIAL_BAUD);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  psnClient.begin(sSerial, newData);

  //subscribe auf alle möglichen Topics
  psnClient.subscribe("*");

  Serial.println("\n\nReady for Clients...");

  

}

void loop() {
  //Verarbeiten der Daten, prüfen ob Netzwerk frei und versenden der Daten
  psnClient.handleNetwork();

  if(newPing){
    newPing = false;
    Serial.print("Empfangene Clients: ");
    for(int i = 0; i < MAX_NR_CLIENTS+1; i++){
      if(recievedPings[i]){
        Serial.print(i);
        Serial.print(" | ");
      }
    }
    Serial.println();
  }
}
