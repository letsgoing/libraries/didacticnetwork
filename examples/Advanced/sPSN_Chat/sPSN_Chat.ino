/**
 * file:  sPSN_Chat.ino
 * author:  letsgoING -> info@letsgoing.de
 *
 * description:
 *   Dieses Programm ist ein Teil eines Beispiels für ein einfaches Pub-Sub-Chat-Netzwerk.
 *
 *   Für dieses Chat-Netzwerk werden mindestens 3 Arduinos benötigt:
 *
 *   Arduino1:   sPSN_Broker.ino
 *              - IR-Sender an Pin 11 IR-Empfänger an Pin10
 *
 *   Arduino2-n: sPSN_Chat.ino (dieses Programm)
 *              - IR-Sender an Pin 11 IR-Empfänger an Pin10
 * Funktion:
 *   Am Arduino2-n kann ein eigner Name (Sende-Topic) angeben werden.
 *   Danach kann angeben werden wem zugehört werden soll (Empfangs-Topics).
 *   Arduino1 (Server) übernimmt das Verteilen der Nachrichten.
 *
 * parameter:
 *   MAX_LEN_PAYLOAD = 20 Zeichen (didacticNet.h)
 *   MAX_LEN_TOPICS = 10 Zeichen (didacticNet.h)
 *   max Laenge der Eingegebenen Daten: 100 Zeichen (5*MAX_LEN_PAYLOAD)
 *
 * date:  13.07.2021
 */
#include <Arduino.h>
#include "SoftwareSerial.h"
#include "DidacticNet.h"

#define SERIAL_BAUD 2400 // lege Geschwindigkeit für serielle Schnittstellen fest

#define TXT_STARTINFO "1. Den eigenen Namen mit einem # eingeben\nund mit Enter bestaetigen. -> \n#DeinName\n2. Die Namen deiner Chatpartner*innen mit einem @ eingeben ->\n@ChatPartner*in \t(mehrfach moeglich)\n3. Chatten:\nPro Nachricht duerfen maximal 20 Zeichen eingegeben werden!\n\nHINWEIS:\nStelle das Zeilenende im SerialMonitor auf \"ZEILENUMBRUCH (CR)\"\n*********************************************************************\n\n"

char readData[MAX_LEN_PAYLOAD * 5] = {'\0'}; // Array für eingelesene Daten (mit Puffer für zu lange Eingaben)

char topicSub[MAX_LEN_TOPICS] = {""}; // Array für neues Empfangs-Topic
char topicPub[MAX_LEN_TOPICS] = {""}; // Array für eigenes Sende-Topic

// Callback-Funktion wird bei neuen Daten automatisch aufgerufen
void newData(char *topic, char *payload)
{
  // Ausgabe-> "Chatname":   "geschriebene Nachricht"
  Serial.print(topic); // topic entspricht im Chat dem NAmen des Chatpartners
  Serial.print(":\t");
  Serial.println(payload); // payload enthält die geschriebene Nachricht
}

SoftwareSerial sSerial(10, 11); // SoftwareSerial an Rx = Pin10 -> Empfänger | Tx = Pin11 -> Sender

DidacticPSNetClient psnClient; // Erzeuge PubSub-Client-Instanz
SerialReader sReader;

void setup()
{

  Serial.begin(SERIAL_BAUD); // Starte Serielle Schnittstelle (zum PC)

  sSerial.begin(SERIAL_BAUD); // Starte SoftwareSerielle Schnittstelle (zu IR-Link-Modulen)

  psnClient.begin(sSerial, newData); // Starte PubSubClient mit SoftwareSerial und Callbackfunktion "clientCallback"
  sReader.begin(Serial);

  // AUSGABE INFOTEXT
  Serial.print(TXT_STARTINFO);
}

void loop()
{

  psnClient.handleNetwork(); // Verarbeiten der Daten, prüfen ob Netzwerk frei und versenden der Daten

  int nrOfAscii = sReader.readSerialData(readData, DN_ASCII_CR); // Einlesen der Nutzereingabe am SerialMonitor (Rueckgabewert = Anzahl der gelesenen Zeichen)

  if (nrOfAscii > 0)
  { // Wenn Daten fertig eingelesen wurden

    // NEUEM CHATPARTNER FOLGEN
    if (readData[0] == '@')
    {                                 // Wenn '@' vorne steht, dann neuer Chatpartner anlegen (neues Topic abonnieren)
      strcpy(readData, &readData[1]); // verschiebe um ein Zeichen (entferne '@')
      psnClient.subscribe(readData);  // Lege fest zu welchem Topic Daten empfangen werden sollen (den Namen des Chatpartners)

      Serial.print("Nachrichten von "); // Ausgabe welches Topic abonniert wurde
      Serial.print(readData);
      Serial.println(" abonniert.");
    }

    // CHATPARTNER ENTFOLGEN
    else if (readData[0] == '!')
    {                                  // Wenn '@' vorne steht, dann neuer Chatpartner anlegen (neues Topic abonnieren)
      strcpy(readData, &readData[1]);  // verschiebe um ein Zeichen (entferne '!')
      psnClient.unsubscribe(readData); // Lege fest zu welchem Topic Daten empfangen werden sollen (den Namen des Chatpartners)

      Serial.print("Nachrichten von "); // Ausgabe welches Topic abonniert wurde
      Serial.print(readData);
      Serial.println(" nicht mehr abonniert.");
    }

    // EIGENEN NAMEN ANLEGEN
    else if (readData[0] == '#')
    {                                 // Wenn '#' vorne steht, dann neuer eigener Name (neues Topic unter dem gesendet wird)
      strcpy(topicPub, &readData[1]); // kopiere das neue Topic (deinen neuen Namen) in das passende Array (+ 1 = entferne '#')
      // strcpy(topicPub, readData + 1); //-> Alternative zur Zeile oberhalb

      Serial.print("Dein Name:\t"); // Ausgabe unter welchem Topic veröffentlicht wird
      Serial.println(topicPub);
    }

    // NACHRICHT VERSCHICKEN (unter eigenem Namen)
    else
    {                                        // Wenn "normale" Nachrichten eingegeben wurden, dann Daten unter eigenem Topic versenden
      psnClient.publish(topicPub, readData); // Bereite eingegebene Daten zum Senden vor

      Serial.print("Ich:\t"); // Ausgabe was unter deinem Topic veröffentlicht wird
      Serial.println(readData);
    }
  }
}