/**
 *file:  sPSN_Client1
 *author:  letsgoING -> info@letsgoing.de
 *
 *description:
 * Dieses Programm ist ein einfaches Beispiel für ein einfaches Pub-Sub-Netzwerk.
 * Für das Netwerk werden 3 Arduinos mit IR-Link-Modulen benötigt:
 *
 * Arduino1: sPSN_Broker.ino
 *
 * Arduino2: sPSN_Client1.ino (dieses Programm)  -> Sendet Wert des Potis | Empfängt Zustand des Tasters
 * (Poti an PinA0 | LED an Pin5)
 *
 * Arduino3: sPSN_Client2.ino -> Sendet Zustand des Tasters | Empfängt Wert des Potis
 * (Taster an Pin2 | LED an Pin5)
 *
 * parameter:
 *  MAX_LEN_PAYLOAD = 20 Zeichen (didacticNet.h)
 *  MAX_LEN_TOPICS = 10 Zeichen (didacticNet.h)
 *
 *date:  06.07.2021
 */
#include <Arduino.h>
#include "SoftwareSerial.h"
#include "DidacticNet.h"

#define SERIAL_BAUD 2400 // lege Geschwindigkeit für serielle Schnittstellen fest

#define LED_PIN 5
#define POTI_PIN A0

#define THRESHOLD 10 // Schwellwert für min. Wertänderung

char topicPublish[MAX_LEN_TOPICS] = "potiNr";  // Topic unter dem (eigene) Daten veröffentlicht werden
char topicSubscribe[MAX_LEN_TOPICS] = "btnNr"; // Topic (von anderem TN) das abboniert werden soll

SoftwareSerial sSerial(10, 11); // Erzeuge SoftwareSerial-Instanz mit Rx = Pin10 -> Empfänger | Tx = Pin11 -> Sender

DidacticPSNetClient psnClient; // Erzeuge PubSub-Client-Instanz

// Callback-Funktion - wird beim Empfang neuer Daten aufgerufen
void newData(char *topic, char *payload)
{
  Serial.print("New data from topic ");
  Serial.print(topic);
  Serial.print(": ");
  Serial.println(payload);

  boolean static stateLED = false;

  // Wechsle Zustand der Variable "stateLED" wenn Taster beim Sender gedrueckt wurde
  if (payload[0] == '1')
  {
    stateLED = !stateLED;
    digitalWrite(LED_PIN, stateLED); // Setze Ausgang entsprechend dem empfangenen Wert
  }
}

void setup()
{

  Serial.begin(SERIAL_BAUD);  // Starte Serielle Schnittstelle (zum PC)
  sSerial.begin(SERIAL_BAUD); // Starte SoftwareSerielle Schnittstelle (zu IR-Link-Modulen)

  pinMode(LED_PIN, OUTPUT);

  psnClient.begin(sSerial, newData); // Starte PubSub Client an SoftwareSerial Schnittstelle

  psnClient.subscribe(topicSubscribe); // Lege fest zu welchem Topic Daten empfangen werden sollen
}

void loop()
{

  psnClient.handleNetwork(); // Verarbeiten der Daten, prüfen ob Netzwerk frei und versenden der Daten

  int currentValue = analogRead(POTI_PIN); // lese Poti ein und speichere Wert
  psnClient.publishOnChange(topicPublish, currentValue, THRESHOLD);
}
