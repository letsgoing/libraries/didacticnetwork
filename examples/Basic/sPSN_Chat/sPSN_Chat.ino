/**
 * file:  sPSN_Chat.ino
 * author:  letsgoING -> info@letsgoing.de
 *
 * description:
 *   Dieses Programm ist ein Teil eines Beispiels für ein einfaches Pub-Sub-Chat-Netzwerk.
 *
 *   Für dieses Chat-Netzwerk werden mindestens 3 Arduinos benötigt:
 *
 *   Arduino1:   sPSN_Broker.ino
 *              - IR-Sender an Pin 11 IR-Empfänger an Pin10
 *
 *   Arduino2-n: sPSN_Chat.ino (dieses Programm)
 *              - IR-Sender an Pin 11 IR-Empfänger an Pin10
 * Funktion:
 *   Am Arduino2-n kann ein eigner Name (Sende-Topic) angeben werden.
 *   Danach kann angeben werden wem zugehört werden soll (Empfangs-Topics).
 *   Arduino1 (Server) übernimmt das Verteilen der Nachrichten.
 *
 * parameter:
 *   MAX_LEN_PAYLOAD = 20 Zeichen (DidacticNet.h)
 *   MAX_LEN_TOPICS = 10 Zeichen (DidacticNet.h)
 *   MAX_LEN_USERINPUT = 41 Zeichen (DidacticNet.h)
 *
 * date:  13.07.2021
 */
#include <Arduino.h>
#include "SoftwareSerial.h"
#include "DidacticNet.h"

#define SERIAL_BAUD 2400 // lege Geschwindigkeit für serielle Schnittstellen fest

#define TXT_STARTINFO "1. Den eigenen Namen mit einem # eingeben\nund mit Enter bestaetigen. -> \n#DeinName\n2. Die Namen deiner Chatpartner*innen mit einem @ eingeben ->\n@ChatPartner*in \t(mehrfach moeglich)\n3. Chatten:\nPro Nachricht duerfen maximal 20 Zeichen eingegeben werden!\n\nHINWEIS:\nStelle das Zeilenende im SerialMonitor auf \"ZEILENUMBRUCH (CR)\"\n*********************************************************************\n\n"

char topicPub[MAX_LEN_TOPICS] = {""}; // Array für eigenes Sende-Topic

SoftwareSerial sSerial(10, 11); // SoftwareSerial an Rx = Pin10 -> Empfänger | Tx = Pin11 -> Sender

DidacticPSNetClient psnClient; // Erzeuge PubSub-Client-Instanz
serialMonitorUI serialUI;      // Vereinfachtes Einlesen von Text und Befehlen per SerialMonitor

void setup()
{

  Serial.begin(SERIAL_BAUD); // Starte Serielle Schnittstelle (zum PC)
  serialUI.begin(Serial);    // Starte UserInterface via SerialMonitor

  sSerial.begin(SERIAL_BAUD); // Starte SoftwareSerielle Schnittstelle (zu IR-Link-Modulen)
  psnClient.begin(sSerial);   // Starte PubSubClient mit SoftwareSerial und Callbackfunktion "clientCallback"

  // AUSGABE INFOTEXT
  Serial.print(TXT_STARTINFO);
}

void loop()
{
  // NETWERK VERWALTEN
  //********************
  psnClient.handleNetwork(); // Verarbeiten der Daten, prüfen ob Netzwerk frei und versenden der Daten

  // NEUE NACHRICHT?
  //********************
  if (psnClient.available()) // Prüfen ob neue Nachricht verfügbar
  {
    char chatName[MAX_LEN_TOPICS] = {0};     // Array um Name von Chatpartner:in zu Speichern
    char chatMessage[MAX_LEN_PAYLOAD] = {0}; // Array um Nachricht von Chatpartner:in zu Speichern

    psnClient.readTopic(chatName);      // Einlesen des zuletzt empfangenen Topics (entspricht im Chat dem Namen)
    psnClient.readPayload(chatMessage); // Einlesen der zuletzt empfangenen Payload (enthält die geschriebene Nachricht))

    // AUSGABE EMPFANGENE NACHRICHT AUF SERIALMONITOR
    Serial.print(chatName);
    Serial.print(":\t");
    Serial.println(chatMessage);
  }

  // NEUE EINGABE?
  //********************
  if (serialUI.available()) // Prüfen ob neue Eingabe am SerialMonitor verfügbar
  {
    char command = serialUI.readCommand(); // Lese Steuerzeichen aus Eingabe aus -> @ / ! / #

    char userInput[MAX_LEN_PAYLOAD + 2];
    serialUI.readUserInput(userInput); // Speichere eingegebenen Text in Array

    // NEUEM CHATPARTNER FOLGEN
    if (command == '@')
    {                                 // Wenn '@' vorne steht, dann neuer Chatpartner anlegen (neues Topic abonnieren)
      psnClient.subscribe(userInput); // Lege fest zu welchem Topic Daten empfangen werden sollen (den Namen des Chatpartners)

      Serial.print("Nachrichten von "); // Ausgabe welches Topic abonniert wurde
      Serial.print(userInput);
      Serial.println(" abonniert.");
    }

    // CHATPARTNER ENTFOLGEN
    else if (command == '!')
    {                                   // Wenn '!' vorne steht, dann eingegebenen Chatpartner entfolgen
      psnClient.unsubscribe(userInput); // Lege fest zu welchem Topic keine Daten mehr empfangen werden sollen (Namen des Chatpartners)

      Serial.print("Nachrichten von "); // Ausgabe welches Topic "entabonniert" wurde
      Serial.print(userInput);
      Serial.println(" nicht mehr abonniert.");
    }

    // EIGENEN NAMEN ANLEGEN
    else if (command == '#')
    {                               // Wenn '#' vorne steht, dann neuer eigener Name (neues Topic unter dem gesendet wird)
      strcpy(topicPub, userInput);  // kopiere das neue Topic (deinen neuen Namen) in das topicPub-Array
      Serial.print("Dein Name:\t"); // Ausgabe unter welchem Topic ab jetzt veröffentlicht wird
      Serial.println(topicPub);
    }

    // NACHRICHT VERSCHICKEN (unter eigenem Namen)
    else
    {                                         // Wenn "normale" Nachrichten eingegeben wurden, dann Daten unter eigenem Topic versenden
      psnClient.publish(topicPub, userInput); // Bereite eingegebene Daten zum Senden vor

      Serial.print("Ich:\t"); // Ausgabe was unter deinem Topic veröffentlicht wird
      Serial.println(userInput);
    }
  }
}